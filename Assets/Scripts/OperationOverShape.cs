﻿public enum OperationOverShape
{
    NONE,
    BLEND,
    CUT,
    MASK
}
