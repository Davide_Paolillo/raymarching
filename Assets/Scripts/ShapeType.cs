﻿public enum ShapeType
{
    CUBE,
    SPHERE,
    TORUS
}
