﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{
    [SerializeField] private ShapeType shapeType;
    [SerializeField] private OperationOverShape operationOverShape;

    [SerializeField] private Color color;
    [Range(0, 1)][SerializeField] private float blendStrength;
    
    private int numberOfChildren;

    public ShapeType ShapeType { get => shapeType; }
    public OperationOverShape OperationOverShape { get => operationOverShape; }
    public Color Color { get => color; set => color = value; }
    public float BlendStrength { get => blendStrength; }
    public int NumberOfChildren { get => numberOfChildren; set => numberOfChildren = value; }
    public Vector3 Position { get => this.transform.position; }
    public Vector3 Scale {
        get
        {
            Vector3 parentScale = Vector3.one;
            if (transform.parent != null && transform.parent.GetComponent<Shape>() != null)
                parentScale = transform.parent.GetComponent<Shape>().Scale;

            return Vector3.Scale(this.transform.localScale, parentScale);
        }
    }
}
