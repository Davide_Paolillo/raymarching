﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Master : MonoBehaviour
{
    [SerializeField] private ComputeShader rayMarchingShader;

    [SerializeField] private Light lightSource;

    private List<ComputeBuffer> computeBuffers;

    private Camera mainCamera;

    private RenderTexture target;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        mainCamera = Camera.main;
        computeBuffers = new List<ComputeBuffer>();

        InitializeRenderTexture();
        GetScene();
        SetShaderParameters();

        rayMarchingShader.SetTexture(0, "Source", source);
        rayMarchingShader.SetTexture(0, "Destination", target);

        int threadGroupX = Mathf.CeilToInt(mainCamera.pixelWidth / 8.0f);
        int threadGroupY = Mathf.CeilToInt(mainCamera.pixelHeight / 8.0f);
        rayMarchingShader.Dispatch(0, threadGroupX, threadGroupY, 1);

        Graphics.Blit(target, destination);

        foreach (ComputeBuffer computeBuffer in computeBuffers)
            computeBuffer.Dispose();
    }

    private void InitializeRenderTexture()
    {
        if (target == null || target.width != mainCamera.pixelWidth || target.height != mainCamera.pixelHeight)
        {
            if (target != null)
                target.Release();

            target = new RenderTexture(mainCamera.pixelWidth, mainCamera.pixelHeight, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            target.enableRandomWrite = true;
            target.Create();
        }
    }

    private void GetScene()
    {
        List<Shape> shapes = new List<Shape>(FindObjectsOfType<Shape>());

        ShapeData[] shapeDataArray = new ShapeData[shapes.Count];

        for (int i = 0; i < shapes.Count; i++)
        {
            shapeDataArray[i] = new ShapeData();
            shapeDataArray[i].position = shapes[i].Position;
            shapeDataArray[i].scale = shapes[i].Scale;
            shapeDataArray[i].color = new Vector3(shapes[i].Color.r, shapes[i].Color.g, shapes[i].Color.b);
            shapeDataArray[i].shapeType = (int)shapes[i].ShapeType;
            shapeDataArray[i].operation = (int)shapes[i].OperationOverShape;
            shapeDataArray[i].blendStrength = shapes[i].BlendStrength;
            shapeDataArray[i].numberOfChildren = shapes[i].NumberOfChildren;
        }

        ComputeBuffer shapesBuffer = new ComputeBuffer(shapeDataArray.Length, ShapeData.GetSize());
        shapesBuffer.SetData(shapeDataArray);
        rayMarchingShader.SetBuffer(0, "_Shapes", shapesBuffer);
        rayMarchingShader.SetInt("_NumberOfShapes", shapeDataArray.Length);

        computeBuffers.Add(shapesBuffer);
    }

    private void SetShaderParameters()
    {
        rayMarchingShader.SetMatrix("_CameraToWorld", mainCamera.cameraToWorldMatrix);
        rayMarchingShader.SetMatrix("_CameraInverseProjection", mainCamera.projectionMatrix.inverse);
        rayMarchingShader.SetVector("_Light", lightSource.transform.forward);
        rayMarchingShader.SetVector("_LightPosition", lightSource.transform.position);
    }
}
